%% UIUC Cars Dataset: Detection Images
% This function shows the detection results in images. This function loads
% the detection results from a specific experiment (e.g brfs_t1) and shows 
% the detections according to a particular threshold (detThr). The images 
% are saved at the ./images/detections/ folder.
function fun_uiuc_cars_detection_images()
clc,clear all,close all

% messages
fun_messages('UIUC Cars Dataset','presentation');
fun_messages('detection curves','title');

% parameters
detPath = './results/brfs_t1/detections/positives/';  % detection files path -experiment brfs_t1-
imgPath = './images/datasets/uiuc_cars/test_images/positives/';  % test images path
annPath = './images/datasets/uiuc_cars/test_images/annotations/';  % annotation files path
detThr = 0.1020;  % detection threshold (e.g EER threshold)
intThr = 0.3;  % intersection overlapping threshold
recThick = 6;  % detection rectangle thickness
annColor = [0,0,1];  % annotation -ground truth- color
posColor = [0,1,0];  % true positive detection color
negColor = [1,0,0];  % false positive detection color

% detection files
detFiles = dir([detPath,'*.mat']);

% num. detection files
numFiles = size(detFiles,1);

% messages
fun_messages(sprintf('num. detection files -> %d',numFiles),'information');

% test files
for iterFile = 1:numFiles
    
    % detection file name
    fileName = detFiles(iterFile).name;
    
    % message
    fun_messages(sprintf('image -> %s',fileName),'information');
    
    % detection data
    detData = fun_data_load(detPath,fileName);
    
    % annotation data -ground truth-
    annData = fun_data_load(annPath,[detData.name,'.mat']);
    
    % annotation data
    annBoxes = annData.boxes;  % bounding boxes -objects annotated in the image-
    numObjects = annData.numBoxes;  % num. bounding boxes -objects-
    
    % thresholding: detections over threshold
    indx = detData.scores>detThr;
    boxes = detData.boxes(indx,:);
    scores = detData.scores(indx,:);
    
    % non-maxima suppression: overlapped detection boxes are removed
    % according to the input intersection -overlapping- threshold.
    newBoxes = fun_non_maxima_suppression(boxes,scores,intThr);
    
    % num. detections
    numDetections = size(newBoxes,1);
    
    % intersection measure: This measures the degree of overlapping
    % -intersection- between the annotation boxes and the detection
    % boxes after non-maxima suppression.
    intValues = fun_boxes_intersection(annBoxes,newBoxes,0.5);
    intIndexes = sum(intValues.indexes,1);
    
    % detection boxes
    fpBoxes = [];   % false positives
    tpBoxes = [];   % true positives
    
    % labeling boxes: true positives or false positives
    for iterDet = 1:numDetections
        % current box
        box = newBoxes(iterDet,:);
        % classification
        if (intIndexes(iterDet)>0), tpBoxes = [tpBoxes;box]; else fpBoxes = [fpBoxes;box]; end
    end
    
    % current image
    cImg = imread([imgPath,detData.name]);
    dImg = fun_image_color(cImg,'RGB');
    
    % show ground thruth
    for iterObject = 1:numObjects
        dImg = fun_show_detections(dImg,annBoxes(iterObject,:),annColor,recThick);
    end
    
    % show false positives
    dImg = fun_show_detections(dImg,fpBoxes,negColor,recThick);
    
    % show true positives
    dImg = fun_show_detections(dImg,tpBoxes,posColor,recThick);
    
    % save image
    imwrite(dImg,sprintf('./images/detections/pos_%s.png',detData.name));
    
end

% message
fun_messages('End','title');
end

%% show detections
function output = fun_show_detections(img,boxes,color,thick)

% num. detection boxes
numBoxes = size(boxes,1);

% detections
for iter = 1:numBoxes
    
    % coordinates
    left   = boxes(iter,1);
    top    = boxes(iter,2);
    right  = boxes(iter,3);
    bottom = boxes(iter,4);
    
    % draw rectangle
    img = fun_draw_rectangle(img,[left,top,right,bottom],color,thick);
    
end

% output
output = img;
end

