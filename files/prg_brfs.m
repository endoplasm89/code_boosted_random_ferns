%% Boosted Random Ferns (BRFs)
%
% Description:
%   This program computes the Boosted Random Ferns classifier (BRFs)
%   used to perfom efficient detection of object categories in images.
%
%   Particularly, the BRFs classifier [3] is computed using Real AdaBoost 
%   in order to select and combine -automatically- the most discriminative 
%   weak classifiers (WCs) and where each one consists of a specific random 
%   fern [1]. The random ferns are computed over local Histograms of
%   Oriented Gradients (HOG) with the goal of increasing its robustness
%   against lighting and intra-class changes.
%
%   In contrast to the original version of BRFs [3], in this program the 
%   BRFs classifier uses shared random ferns [4] to speed up the detection 
%   step since the feature computation -convolution of ferns in the image- 
%   is done in advance and independent of the number of weak classifiers. 
%
%   For more detail about the BRFs classifier refer to references [2][3][4].
%
% Comments:
%   The parameters of the classifier can be found in the fun_parameters.m 
%   function (/files/functions/). The fun_experiments.m function file allows
%   to compute and evaluate different classifiers, each one computed with 
%   different parameters.
%
%   For evaluation, the UIUC Cars Dataset [5] is used. This dataset contains
%   108 testing images with cars -side view- at multiple scales. For training,
%   500 positive and negative imageas are used. If anyone uses this dataset, 
%   please cite the following references [5][6].
%
%   If you make use of this code for research articles, we kindly encourage
%   to cite the references [2][3][4], listed below. This code is only for 
%   research and educational purposes.
%
% Steps:
%   Steps to exucute the program:
%     1. Run the prg_setup.m file to configure the program paths.
%     2. Run the prg_brfs.m file (/files/) to compute the classifier and to
%        perfom detection over the given dataset. 
%     3. Run the fun_uiuc_cars_detection_performance.m (/files/datasets/) 
%        to compute the detection performance plots of the classifier and 
%        to determine the detection threshold (EER threshold).
%     4. Run the fun_uiuc_cars_detection_images.m file to observe the 
%        detection results in the images. The images are saved at the
%        /images/detections/ folder. Set the detection threshold (detThr)
%        in accordance to the EER.
%
% References:
%   [1] Fast keypoint recognition in ten lines of code. M. Ozuysal, P. Fua,
%       V. Lepetit. Computer Vision and Pattern Recognition (CVPR), 2007.
%
%   [2] Bootstrapping Boosted Random Ferns for Discriminative and Efficient
%       Object Classification. M. Villamizar, J. Andrade-Cetto, A. Sanfeliu
%       and F. Moreno-Noguer. Pattern Recognition, 2012.
%
%   [3] Efficient Rotation Invariant Object Detection using Boosted Random 
%       Ferns. M. Villamizar, F. Moreno-Noguer, J. Andrade-Cetto and 
%       A. Sanfeliu. Conference on Computer Vision and Pattern Recognition
%       (CVPR), San Francisco, USA, June 2010.
%
%   [4] Shared Random Ferns for Efficient Detection of Multiple Categories.
%       M. Villamizar, F. Moreno-Noguer, J. Andrade-Cetto and A. Sanfeliu.
%       International Conference on Pattern Recognition (ICPR). Istanbul, 
%       Turkey, August 2010.
%   [5] Shivani Agarwal, Aatif Awan, and Dan Roth, Learning to detect 
%       objects in images via a sparse, part-based representation.
%       IEEE Transactions on Pattern Analysis and Machine Intelligence, 
%       26(11):1475-1490, 2004.
%   [6] Shivani Agarwal and Dan Roth, Learning a sparse representation for 
%       object detection. In Proceedings of the Seventh European Conference 
%       on Computer Vision, Part IV, pages 113-130, Copenhagen, Denmark, 2002.
%
% Contact: 
%   Michael Villamizar
%   mvillami@iri.upc.edu
%   http://www.iri.upc.edu/people/mvillami/
%   Institut de Robòtica i Informàtica Industrial CSIC-UPC
%   Barcelona - Spain
%   2014
%

%% main function
% The program computes, shows and tests the object classifier for different
% experiments, where each one considers different program parameters (e.g. 
% number of weak classifier or feature space). When the experiment is
% finished the classifier, the detection results and the temporal variables
% are saved in the results folder.
function prg_brfs()
clc, close all,clear all

% message
fun_messages('Boosted Random Ferns (BRFs)','presentation');

% global variables
global iterExperiment;

% experiments
for iterExperiment = 1:1
   
    % train the classifier -clfr-
    clfr = fun_train();
    
    % show classifier
    fun_show_classifier(clfr);
    
    % test the classifier
    fun_test(clfr);
    
    % copy and save results
    fun_save_results();
    
end

% message
fun_messages('End','title');
end









