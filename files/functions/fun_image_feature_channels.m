%% image feature channels
% This function returns the number of feature channels in images. For
% example, a RGB image has three channels, corresponding to red, green and
% blue. For the case of histograms of oriented gradients (HOG), the output
% value is the number of gradient orientation channels or bins.
function output = fun_image_feature_channels()
    
% parameters
prms = fun_parameters();  % program parameters
numBins = prms.imgData.numOriBins;  % num. gradient orientation bins
imgSpace = prms.imgData.imgSpace;  % image feature space

% num. channels according to the image space
switch (imgSpace)
    case 'HOG'  % histogram of oriented gradients
        numChannels = numBins;
    case 'GRAY' % gray-scaled image
        numChannels = 1;
    case 'RGB'  % color -rgb- image
        numChannels = 3;
    case 'HSV'  % color -hue,saturation,value- image
        numChannels = 3;
    otherwise
        fun_messages('incorrect image feature space','error');
end
   
% output
output = numChannels;
end
