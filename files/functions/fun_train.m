%% train the classifier 
% This function computes the object classifier using a training dataset.
function output = fun_train()
    
% message
fun_messages('train','title');
        
% load/compute the object classifier
try
    
    % load the previous classifier
    clfr = fun_data_load('./variables/','classifier.mat');
    
    % message
    fun_messages('the classifier was loaded successfully','information');
    
catch ME
    
    % random ferns computation
    ferns = fun_random_ferns();
        
    % training samples
    samples = fun_samples();
    
    % Boosted Random Ferns (BRFs)
    clfr = fun_classifier_brfs(samples,ferns);
    
    % save classifier
    fun_data_save(clfr,'./variables/','classifier.mat');
    
end

% output
output = clfr;
end
