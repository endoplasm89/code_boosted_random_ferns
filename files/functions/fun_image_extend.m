%% extend image
% This function extends the image in order to consider mild occlusions 
% during the detection. The added information corresponds to border lines 
% of the original image.
function output = fun_image_extend(img,ext)
if (nargin~=2), fun_messages('incorrect input parameters','error'); end

% image color
if (size(img,3)~=3)
    
    % gray-level image
    outImg = img;
    
    % extension
    for iter = 1:ext
        left   = outImg(:,1);
        right  = outImg(:,end);
        outImg = [left,outImg,right];
        top    = outImg(1,:);
        bottom = outImg(end,:);
        outImg = [top;outImg;bottom];
    end
else
    
    % color image
    outImg = img;
    
    % extension
    for iter = 1:ext
        left   = outImg(:,1,:);
        right  = outImg(:,end,:);
        outImg = [left,outImg,right];
        top    = outImg(1,:,:);
        bottom = outImg(end,:,:);
        outImg = [top;outImg;bottom];
    end
end

% output
output = outImg;
end
