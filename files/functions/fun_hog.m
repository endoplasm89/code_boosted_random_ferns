%% Histogram of Oriented Gradients (HOG)
% This function computes the histogram of oriented gradients over
% the input image -img-. The image is resized according to the input size.
function output = fun_hog(img,imgSize)
if (nargin~=2), fun_messages('incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % program parameters                                   
cellSize = prms.imgData.cellSize;  % cell size -pixels x pixels-                                   
numOriBins = prms.imgData.numOriBins;  % num. gradient orientations                      

% image resize
newSize = imgSize(1:2)*cellSize;
%img = imresize(img,newSize,'bilinear');
img = imresize(img,newSize);

% gray-scaled image
img = fun_image_color(img,'GRAY');

% HOG computation -mex file-
HOG = mex_HOG(img,numOriBins,cellSize);

% output
output = HOG;
end
