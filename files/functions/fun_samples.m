%% samples
% This function extracts the training samples used to compute the object 
% classifier. The object dataset includes positive -object- and negative 
% -background- image samples.
function output = fun_samples()

% message
fun_messages('image samples','process');

% load/extract image samples
try 
    
    % load previous training samples
    samples = fun_data_load('./variables/','samples.mat');
    
    % message
    fun_messages('the training samples were loaded successfully','information');

catch ME
    
    % parameters
    prms = fun_parameters();                    % program parameters
    imgSize = prms.classifier.objSize;          % image size -object size-
    imgSpace = prms.imgData.imgSpace;           % image feature space
    posImgFmt = prms.train.posImgFmt;           % positive image format
    negImgFmt = prms.train.negImgFmt;           % negative image format
    numPosImgs = prms.train.numPosImgs;         % num. positive images
    numNegImgs = prms.train.numNegImgs;         % num. negative images
    posImgPath = prms.train.posImgPath;         % positive images path
    negImgPath = prms.train.negImgPath;         % engative images path
    
    % load positive and negative images
    posImgs = fun_load_images(posImgPath,numPosImgs,posImgFmt,imgSize);
    negImgs = fun_load_images(negImgPath,numNegImgs,negImgFmt,imgSize);
    
    % num. positive and negative samples
    numPosSamples = size(posImgs,4);
    numNegSamples = size(negImgs,4);
    
    % image samples
    samples.imgSize = imgSize;
    samples.imgSpace = imgSpace;
    samples.positives = posImgs;
    samples.negatives = negImgs;
    samples.numPosSamples = numPosSamples;
    samples.numNegSamples = numNegSamples;
    
    % save
    fun_data_save(samples,'./variables/','samples.mat');
    
end

% message
fun_messages(sprintf('num. positive image samples: %d',samples.numPosSamples),'information');
fun_messages(sprintf('num. negative image samples: %d',samples.numNegSamples),'information');
fun_messages(sprintf('image size: [%d %d]',samples.imgSize(:)),'information');
fun_messages(sprintf('image space: %s',samples.imgSpace),'information');

% output
output = samples;
end
