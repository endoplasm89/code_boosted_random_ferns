%% save results
% This function saves the experiment results in a particular folder.
function fun_save_results()
    
% current experiment
prmsExp = fun_experiments();
  
% output directory    
mkdir(sprintf('./results/%s',prmsExp.tag));    
mkdir(sprintf('./results/%s/detections/',prmsExp.tag));    
mkdir(sprintf('./results/%s/detections/positives/',prmsExp.tag));    
mkdir(sprintf('./results/%s/detections/negatives/',prmsExp.tag));    
mkdir(sprintf('./results/%s/variables/',prmsExp.tag));    

% move variables
movefile('./variables/*.mat',sprintf('./results/%s/variables/',prmsExp.tag));

% detection files
posFiles = dir('./detections/positives/*.mat');
negFiles = dir('./detections/negatives/*.mat');

% move detection files
if (size(posFiles,1)>0), movefile('./detections/positives/*.mat',sprintf('./results/%s/detections/positives/',prmsExp.tag)); end
if (size(negFiles,1)>0), movefile('./detections/negatives/*.mat',sprintf('./results/%s/detections/negatives/',prmsExp.tag)); end

end
