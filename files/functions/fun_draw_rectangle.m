%% draw rectangle
% This function draws a rectangle on the image.
function output = fun_draw_rectangle(img,location,color,thick)
if (nargin~=4), fun_messages('incorrect input parameters','error'); end

% location 
location = round(location);

% check image limits
if (isempty(location)==0)
    
    % image size
    sy = size(img,1);
    sx = size(img,2);
    
    % rectangle coordinates
    left = location(1);
    top = location(2);
    right = location(3);
    bottom = location(4);
    
    % horizontal lines
    for iterX = min(max(1,left),sx):min(max(1,right),sx)
        for iterThick = 0:thick-1
            tmpa = min(max(1,top+iterThick),sy);
            tmpb = min(max(1,bottom+iterThick),sy);
            img(tmpa,iterX,:) = color;
            img(tmpb,iterX,:) = color;
        end
    end 
    
    % vertical lines
    for iterY = min(max(1,top),sy):1:min(max(1,bottom),sy)
        for iterThick = 0:thick-1
            tmpa = min(max(1,left+iterThick),sx);
            tmpb = min(max(1,right-iterThick),sx);
            img(iterY,tmpa,:) = color;
            img(iterY,tmpb,:) = color;
        end
    end 
    
end

% output
output = img;
end

