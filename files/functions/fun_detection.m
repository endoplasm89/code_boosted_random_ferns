%% detection
% This function performs object detection. The classifier is tested in the
% input image using a sliding window approach. This function makes use of
% mex files for speeding up the detection process.
function output = fun_detection(img,clfr,detThr)
if (nargin~=3), fun_messages('incorrect input parameters','error'); end

% parameters
prms = fun_parameters();  % parameters
imgExt = prms.runtime.imgExt;  % additional image extend
minCell = prms.runtime.minCellSize;  % min. cell size (HOG)
maxCell = prms.runtime.maxCellSize;  % max. cell size (HOG)
imgHeight = prms.runtime.imgHeight;  % standard image height
numMaxDets = prms.runtime.numMaxDets; % num. max. detections
minImgSize = prms.runtime.minImgSize;  % min. image size
numOutputs = prms.runtime.numMaxOutputs;  % num. max. classifier outputs
numImgLevels = prms.runtime.numImgLevels; % num. image levels (pyramid)

% object size
objSize = clfr.objSize;

% extend the image -for mild occlusions-
img = fun_image_extend(img,imgExt);

% resize the image
rszFact = size(img,1)/imgHeight; % resize factor
img = imresize(img,inv(rszFact)); % resize image
imgSize = size(img);  % scaled image size

% num. octaves and scales
numOctaves = min(floor(log2(imgSize(1:2)) - log2(minImgSize)));
numScales = numOctaves*numImgLevels;

% variables
count = 0;  % counter
timImg = 0;  % times: image processing    
timRFs = 0;  % times: feature computacion 
timClfr = 0;  % times: classifier testing

% allocate
detBoxes = zeros(numMaxDets,4);  % detection boxes
detScores = zeros(numMaxDets,1);  % detection scores

% initial time 
t0 = cputime;

% image scales
for iterScale = 0:numScales
   
    % time variable
    t1 = cputime;
    
    % image size
    scaRatio = 2^(iterScale/numImgLevels);  % scale ratio
    newSize = round(imgSize./scaRatio);  % new image size
    
    % scaled image
    %scaImg = imresize(img,newSize(1:2),'bilinear');
    scaImg = imresize(img,newSize(1:2));
   
    % feature image
    featImg = fun_feature_image(scaImg,size(scaImg));
   
    % integral image computation
    II = mex_img2II(featImg);
    
    % update image processing time
    timImg = timImg  + cputime - t1;
    
    % image cells
    for iterCell = minCell:maxCell
        
        % scale factor
        scaFact = iterCell*scaRatio*rszFact;
       
        % current image level
        t1 = cputime; 
        cImg = mex_II2Img(II,iterCell);
        timImg = timImg  + cputime - t1;
        
        % small image size
        if (size(cImg,1)<minImgSize || size(cImg,2)<minImgSize), break; end
        
        % fern maps
        t1 = cputime;
        fernMaps = mex_fern_maps(cImg,clfr.ferns.data,clfr.ferns.fernSize);
        timRFs = timRFs  + cputime - t1;
         
        % test classifier
        t1 = cputime;
        [outputs, ~] = mex_classifier_test(fernMaps,clfr.WCs,clfr.hstms,objSize,numOutputs);
        timClfr = timClfr  + cputime - t1;
       
        % detections
        for iterDet = 1:numOutputs
            
            % detection score
            score = outputs(iterDet,3);  
            
            % this test rejects detections with lower scores
            if (score<detThr), break; end
          
            % update counter
            count = count + 1;
            
            % detection location
            y = outputs(iterDet,1);
            x = outputs(iterDet,2);
            
            % bounding box
            box = round([x, y, x + objSize(2) - 1, y + objSize(1) - 1]*scaFact - imgExt);
         
            % detection data: score and bounding box
            detBoxes(count,:) = box;
            detScores(count,:) = score;
            
        end
    end
end

% sort detections by score
detBoxes = detBoxes(1:count,:);
detScores = detScores(1:count,:);
detData = sortrows([detScores,detBoxes],-1);
detBoxes = detData(:,2:5);
detScores = detData(:,1);

% total time
timTotal = cputime - t0;

% time data
timData.ferns = timRFs;
timData.image = timImg;
timData.total = timTotal;
timData.classifier = timClfr;

% output detections
dets.times = timData;
dets.boxes = detBoxes; 
dets.scores = detScores; 
dets.numDets = count;

% output
output = dets;
end
