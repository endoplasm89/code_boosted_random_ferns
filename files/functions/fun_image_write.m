%% write image
% This function writes the image at the corresponding path and name.
function fun_image_write(img,path,name,number)
if (nargin~=4), fun_messages('incorrect input parameters','error'); end

% adding zeros to the number
txt = fun_zeros(number);

% write
imwrite(img,sprintf('%s%s_%s%.0f.png',path,name,txt,number));

end

