%% experiments
% This function defines all the experiments used in the program. At each
% experiment, the classifier can be tested with diffeent program
% parameters (see fun_parameters.m).
function output = fun_experiments()

% global variables
global iterExperiment;

% experiments
switch (iterExperiment)
    
    case 1
        data.tag = 'brfs_t1';                                               % experiment tag
        data.size = [12,30];                                                % object size
        data.numWCs = 300;                                                  % num. weak classifiers
        data.numFerns = 10;                                                 % num. random ferns
        data.numFeats = 8;                                                  % num. binary features per fern
        data.fernSize = 4;                                                  % fern size
        data.imgSpace = 'HOG';                                              % image feature space
   
    otherwise
        fun_messages('no more experiments','error');
end

% output
output = data;
end
