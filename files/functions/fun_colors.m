%% colors
function output = fun_colors(index)
if (nargin~=1), fun_messages('incorrect input parameters','error'); end

% select color
switch (index)
    case 1
        color = [1.0,0.0,0.0];
    case 2
        color = [0.0,1.0,0.0];
    case 3
        color = [0.0,0.0,1.0];
    case 4
        color = [1.0,1.0,0.0];
    case 5
        color = [0.0,1.0,1.0];
    case 6
        color = [1.0,0.0,1.0];
    case 7
        color = [1.0,0.5,0.8];
    case 8
        color = [0.5,1.0,0.8];
    case 9
        color = [0.8,0.5,1.0];
    case 10
        color = [0.3,0.7,0.3];
    case 11
        color = [0.7,0.3,0.3];
    case 12
        color = [0.3,0.3,0.7];
    case 13
        color = [0.8,0.4,0.0];
    case 14
        color = [0.0,0.8,0.4];
    case 15
        color = [0.4,0.0,0.8];
    case 16
        color = [0.8,0.0,0.4];
    case 17
        color = [0.8,0.8,0.8];
    case 18
        color = [0.2,0.2,0.2];
    case 19
        color = [0.5,0.5,1.0];
    case 20
        color = [1.0,0.5,0.5];
    otherwise
        color = [0.5,1.0,0.5];
        fun_messages('no more colors available','warning');
end

% output
output = color;
end